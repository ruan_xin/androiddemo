package com.ruan.xin.demo;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private  MainListAdapter mainListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView listView = (ListView) findViewById(R.id.listView);
        mainListAdapter = new MainListAdapter(new ArrayList<App>(),this);
        listView.setAdapter(mainListAdapter);
        // Download Top App list
        (new AsyncListViewLoader()).execute("https://42matters.com/api/1/apps/top.json?access_token=1e59f96f9f03b9abb57f212f2123a55c69c46eea&limit=10");
        // Init ImageLoader
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .build();

        ImageLoader.getInstance().init(config);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class AsyncListViewLoader extends AsyncTask<String, Void, List<App>> {
        private final ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPostExecute(List<App> result) {
            super.onPostExecute(result);
            dialog.dismiss();
            mainListAdapter.setItemList(result);
            mainListAdapter.notifyDataSetChanged();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Downloading App List...");
            dialog.show();
        }

        @Override
        protected List<App> doInBackground(String... params) {
            List<App> result = new ArrayList<App>();

            HttpURLConnection conn = null;
            try {
                URL u = new URL(params[0]);
                conn = (HttpURLConnection) u.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-length", "0");
                conn.setUseCaches(false);
                conn.setAllowUserInteraction(false);
                conn.connect();
                InputStream is = conn.getInputStream();
                int status = conn.getResponseCode();
                switch (status) {
                    case 200:
                    case 201:
                        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        StringBuilder sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        br.close();
                        String JSONResp = sb.toString();
                        JSONObject JSONObj = new JSONObject(JSONResp);

                        JSONArray arr = JSONObj.getJSONArray ("appList");

                        for (int i=0; i < arr.length(); i++) {
                            result.add(convertContact(arr.getJSONObject(i)));
                        }
                        return result;
                }

            }
            catch(Throwable t) {
                t.printStackTrace();
            } finally {
                if (conn != null){
                    try{
                        conn.disconnect();
                    } catch (Exception ex){

                    }
                }
            }
            return null;
        }

        private App convertContact(JSONObject obj) throws JSONException {
            String title = obj.getString("title");
            String cat = obj.getString("category");
            Double rate =  obj.getDouble("rating");
            String number_ratings = obj.getString("number_ratings");
            String icon_uri = obj.getString("icon").replace("https", "http");
            return new App(title, cat, rate, number_ratings, icon_uri);
        }

    }
}
