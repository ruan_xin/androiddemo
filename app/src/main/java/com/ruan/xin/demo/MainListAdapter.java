package com.ruan.xin.demo;

import android.animation.ValueAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dd.CircularProgressButton;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by xin on 14/08/15.
 */
public class MainListAdapter extends ArrayAdapter<App> {
    private List<App> itemList;
    private Context context;

    public MainListAdapter(List<App> itemList, Context ctx) {
        super(ctx, R.layout.listitem_layout, itemList);
        this.itemList = itemList;
        this.context = ctx;
    }

    public int getCount() {
        if (itemList != null)
            return itemList.size();
        return 0;
    }

    public App getItem(int position) {
        if (itemList != null)
            return itemList.get(position);
        return null;
    }

    public long getItemId(int position) {
        if (itemList != null)
            return itemList.get(position).hashCode();
        return 0;
    }
    static class ViewHolder {
        ImageView icon;
        TextView title;
        TextView cat;
        TextView rate;
        CircularProgressButton dBtn;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.listitem_layout, null);
            holder = new ViewHolder();
            holder.title = (TextView) v.findViewById(R.id.title);
            holder.cat = (TextView) v.findViewById(R.id.cat);
            holder.rate = (TextView) v.findViewById(R.id.rate);
            holder.icon = (ImageView) v.findViewById(R.id.icon);
            holder.dBtn = (CircularProgressButton) v.findViewById(R.id.downloadBtn);
            v.setTag(holder);
        }else{
            holder = (ViewHolder)v.getTag();
            holder.dBtn.setProgress(0);
        }
        App c = itemList.get(position);
        holder.title.setText(c.getTitle());
        holder.cat.setText(c.getCat());
        holder.rate.setText("Rating:" + String.format("%.2f", c.getRate()) + " (" + c.getNumber_ratings() + ")");

        ImageLoader imageLoader = ImageLoader.getInstance();
        try {
            imageLoader.displayImage(c.getIcon_uri(), holder.icon);
        } catch (Throwable t) {
            t.printStackTrace();
        }
        holder.dBtn.setIndeterminateProgressMode(true);
        holder.dBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CircularProgressButton b = (CircularProgressButton)v;
                if (b.getProgress() == 0) {
                    //b.setProgress(100);
                    simulateSuccessProgress(b);
                } else {
                    b.setProgress(0);
                }
            }
        });
        return v;

    }

    public List<App> getItemList() {
        return itemList;
    }

    public void setItemList(List<App> itemList) {
        this.itemList = itemList;
    }

    private void simulateSuccessProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
            }
        });
        widthAnimation.start();
    }
}
