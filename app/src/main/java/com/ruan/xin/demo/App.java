package com.ruan.xin.demo;

/**
 * Created by xin on 14/08/15.
 */
public class App {
    private String title;
    private String cat;
    private Double rate;
    private String number_ratings;
    private String icon_uri;

    public String getIcon_uri() {
        return icon_uri;
    }

    public void setIcon_uri(String icon_uri) {
        this.icon_uri = icon_uri;
    }

    public String getNumber_ratings() {
        return number_ratings;
    }

    public void setNumber_ratings(String number_ratings) {
        this.number_ratings = number_ratings;
    }

    public App(String title, String cat, Double rate, String number_ratings, String icon_uri) {
        this.title = title;
        this.cat = cat;
        this.rate = rate;
        this.number_ratings = number_ratings;
        this.icon_uri = icon_uri;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
